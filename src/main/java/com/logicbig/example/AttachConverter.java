package com.logicbig.example;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

//@FacesConverter(value = "attachConverter")
@ManagedBean
@RequestScoped
public class AttachConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext fc, UIComponent comp, String value) {
        DualListModel<Attachments> model = (DualListModel<Attachments>) ((PickList) comp).getValue();
        for (Attachments attachments : model.getSource()) {
            if (attachments.getId().equals(value)) {
                return attachments;
            }
        }
        for (Attachments attachments : model.getTarget()) {
            if (attachments.getId().equals(value)) {
                return attachments;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent comp, Object value) {
        return ((Attachments) value).getId();
    }
}
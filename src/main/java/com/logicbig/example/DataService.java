package com.logicbig.example;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataService {

    public Map<Integer, Double> getLineChartData() {
        Map<Integer, Double> map = new LinkedHashMap<>();
        map.put(1, 5.20);
        map.put(2, 19.63);
        map.put(3, 59.01);
        map.put(4, 139.76);
        map.put(5, 300.4);
        map.put(6, 630.0);
       return map;
    }

    //initial unselected list
    List<Attachments> getSourceList (){
        return Arrays.asList(
                new Attachments("1", "Załacznik1", "1"),
                new Attachments("2", "Załacznik2", "3"),
                new Attachments("3", "Załacznik3", "2"),
                new Attachments("4", "Załacznik4", "5")
        );
    }

    //initial selected list
    List<Attachments> getDestinationList(){
        List<Attachments> destinationList = new ArrayList<>();
        destinationList.add(new Attachments("5", "Załaącznik5", "4"));
        return destinationList;
    }
}
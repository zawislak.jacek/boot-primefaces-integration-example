package com.logicbig.example;

public class Attachments {
    private String id;
    private String name;
    private String order;

    public Attachments(String id, String name, String order) {
        this.id = id;
        this.name = name;
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public String getOrder() {
        return order;
    }

    public String getId() {
        return id;
    }
}
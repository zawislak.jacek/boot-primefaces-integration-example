package com.logicbig.example;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;

@Component
@ManagedBean
@ViewScoped
public class PickListBean {

    @Autowired
    private DataService dataService;
    private DualListModel<Attachments> listModel;

    @PostConstruct
    public void init() {
        listModel = new DualListModel<>(new ArrayList<>(dataService.getSourceList()), dataService.getDestinationList());
    }

    public DualListModel<Attachments> getListModel(){
        return listModel;
    }

    public void setListModel(DualListModel<Attachments> listModel) {
        this.listModel = listModel;
    }
}